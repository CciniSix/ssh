支持Debian、Ubuntu、CentOS系统。
运行以下命令：

Ubuntu系统/Debian系统
wget https://gitlab.com/CciniSix/ssh/-/raw/main/sshport.sh && bash sshport.sh

CentOS系统:
wget https://gitlab.com/CciniSix/ssh/-/raw/main/Csshport.sh && bash Csshport.sh


输入端口确认。再打开防火墙端口:

#如果防火墙使用的iptables（Centos 6），修改端口为8080

iptables -I INPUT -p tcp --dport 8080 -j ACCEPT
service iptables save
service iptables restart

#如果使用的是firewall（CentOS 7）

firewall-cmd --zone=public --add-port=8080/tcp --permanent 
firewall-cmd --reload


最后重启ssh生效:
#Debian/Ubuntu系统
 service ssh restart

#CentOS系统
 service sshd restart

